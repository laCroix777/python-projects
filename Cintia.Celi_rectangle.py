##############################################################################
##                 Rectangle and Point classes Assignment                   ##
## Python ArithmeticError                                                   ##
## 02/04/2020                                                               ##
## By. Cintia Celi                                                          ##
## This program creates:                                                    ##
##    1. point objects using the Point class                                ##
##    2. rectangle objects using the Rectangle class                        ##  
##############################################################################

##  Objects in Python are simply named collections of attributes.
##  Each attribute is a reference to another object. 
##  You should think of a variable as being a label, pointer or reference
##  to an object which lives elsewhere in memory.
  
class Point:
    def __init__(self, x, y):
        self.__x = x #X coordinate of the point, p
        self.__y = y #Y coordinate of the point, p

    def __str__(self):
        #return "x={} y={}".format(self.__x, self.__y)
        return f"x={self.__x} y={self.__y}"
    
    @property
    #property to return the x-coordinate
    def x(self):
         return self.__x
        
    #property to return the y-coordinate
    @property
    def y(self):
         return self.__y
     
     ###move to new location (x+dx, y+dy)
    def translate(self,dx,dy):
        self.__x = (self.__x + dx)
        self.__y = (self.__y + dy)

class Rectangle:
    #Static variable. All variable defined on the class level are considered static.
    DEFAULT_WIDTH = 1
    DEFAULT_HEIGHT = 1 
    rectangleCount = 0
    
    def __init__(self, topPoint, width, height):

        self.__topLeft = topPoint

        if width <= 0:
            print("Width cannot be negative or zero. Setting it to the default value of 1")
            self.__width = Rectangle.DEFAULT_WIDTH
        else:
            self.__width = width

        if height <= 0:
            print("Height cannot be negative or zero. Setting it to the default value of 1")
            self.__height = Rectangle.DEFAULT_HEIGHT
        else:
            self.__height = height

        Rectangle.rectangleCount += 1

    def __str__(self):
        return f"topPoint: {self.__topLeft} width={self.__width} height={self.__height}"

    ## Three properties
    @property
    def topLeft(self):
        return self.__topLeft

    @topLeft.setter
    def topLeft(self, topLeft):
        self.__topLeft = topLeft
    
    @property 
    def width(self):
        return self.__width

    @width.setter
    def width(self, width):
        if width <= 0:
            print("Width value must be greater than 0") #raise ValueError
        else:
            self.__width = width
    
    @property
    def height(self):
        return self.__height

    @height.setter
    def height(self, height):
        if height <= 0:
            print("Height value must be greater than 0") #raise ValueError
        else:
            self.__height = height

    @property
    def bottomRight(self):
        bottomXCoord = (self.topLeft.x + self.width)
        bottomYCoord = self.topLeft.y + self.height
        bottomRight = Point(bottomXCoord, bottomYCoord)
        return bottomRight

    @property ###this property returns the area of the rectangle
    def area(self):
        area = (self.width * self.height)
        return area

    @property ###this property returns the perimeter of the rectangle
    def perimeter(self):
        __perimeter = ((self.width)*2 + (self.height)*2)
        return __perimeter

    ###move to new location (x+dx, y+dy)
    def translate(self, dx, dy):
        self.topLeft.translate(dx,dy)
        
def main():
    point1 = Point(6, 25)
    rectangle1 = Rectangle(point1,0,0)

    
    print(point1)
    point1.translate(4,4)
    print(point1)    
    print(rectangle1.topLeft)
    print(rectangle1.bottomRight)
    print(rectangle1.area)
    print(rectangle1.perimeter)

    print("this is the translate for the rectangle")
    print(rectangle1)
    rectangle1.translate(2,2)
    print(rectangle1)
  
if (__name__ == "__main__"):
     main()
