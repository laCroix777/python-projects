#Simplistic Dog class
class Dog:
    dogCount = 0 
    #constructor that initializes 3 attributes
    def __init__(self, name, color, weight=10):

        Dog.dogCount += 1
        
        #Attributes:
        # name: name of the dog
        self.name = name
        # color: color of the dog
        self.color = color
        # isHungry: attribute that tells if the dog is hungry
        self.isHungry = True #isHungry
        # weight: attribute to hold the weight of the dog in KG
        self.weight = weight

    #METHODS:
    # bark: a method that takes no parameters and prints a string of the form
    # "name: Woof Woof” where name is the name of the dog.
    def bark(self):
        print(self.name + ": Woof Woof \n")

    # eat: a method that takes no parameters. This method sets the isHungry
    # attribute to False and adds 100gms to the weight of the dog. It then prints a
    # string of the form “name: Chomp Chomp”.
    def eat(self):
        self.isHungry = False
        self.weight += 0.100
        print(self.name + ": Chomp Chomp\n")    
    
    # walk: a method that takes no parameters.
    def walk(self): 
        # This method will check if the dog is hungry, checks if the dog is hungry
        # if so then it just calls the bark() method
        # Else it decreases the weight of the dog by 100gms, sets the isHungry
          # attribute to True and prints a string of the form “name: Step Step”
          # where name is the name of the dog.
        if self.isHungry == True:
            self.bark()
        else:
            self.isHungry = True
            self.weight -= 0.100
            print(self.name + ": Step Step\n")    
          
    def printStatus(self):
        if self.isHungry != True:
            print(self.name + " is color " + self.color, ", weights", self.weight, "kg(s), and is not hungry")
        else:
            print(self.name + " is color " + self.color, ", weights", self.weight, "kg(s), and is hungry")  
          
def main():
    d1 = Dog("Willie", "brown", 15)
    line = "-"*40
    print(d1.name + "welcomes you! Woof Woof")
    print(line)
    
    cont = True
    while cont:    
        prompt = input('''Enter the command
'S' to get Status enquiry, \t 'F' to feed the dog,
'W' to take it for a walk, \t 'Q' to exit:\n''').upper()
        
        if prompt == "S":
            d1.printStatus()
        elif prompt == "F":
            d1.eat()
        elif prompt == "W":
            d1.walk()
        elif prompt == "Q":
            print("Good bye! Woof woof")
            cont = False           

if __name__ == "__main__":
    main()
    

    
