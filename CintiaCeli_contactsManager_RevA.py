# Program to help user manage contacts information
# Supports
#   listing all contacts,
#   viewing/adding/deleting a contact,
#   printing a given field for all contacts.
# Program uses dictionary of dictionaries to hold the contacts information.

def listContact(contacts):
    counter = 1
    for key in contacts:
        print(str(counter) + str('.'), key)
        counter += 1
    print()
    
def viewContact(contacts):
    contact = input("Enter the name:\t")
    print("Viewing contact for", contact,":\t")

    if contact in contacts:
        print("Viewing contact for", contact,":\t")
    else:
        print("No contact found for that name\n")
        contact = input("Enter the name:\t")
    
    key = "address"
    if key in contacts[contact]:
        address = contacts[contact][key]
        print("\taddress:\t",address, end='\n')
        
    key = "state"
    if key in contacts[contact]:
        state = contacts[contact][key]
        print("\tstate:\t", state, end='\n')
        
    key = "company"
    if key in contacts[contact]:
        company = contacts[contact][key]
        print("\tcompany:\t",company, end='\n')
        
    key = "mobile" #or "landline
    if key in contacts[contact]:
        mobile = contacts[contact][key]
        print("\tmobile:\t", mobile, end='\n')
        
    key = "landline"
    if key in contacts[contact]:
        landline = contacts[contact][key]
        print("\tlandline:\t", landline, end='\n')
    
def addContact(contacts):

    contact=input("Enter the name for the new contact:\t")
    
    if contact not in contacts:
        address=input("Enter the address for the new contact:\t")
        mobile=input("Enter the mobile for the new contact:\t")
        company=input("Enter the company for the new contact:\t")

        contacts[contact] = address
        contacts[contact] = company
        contacts[contact] = mobile
        print("\nName was added")
 
    else:
        print("Contact already exists")    

def delContact(contacts):
    contact = input("Enter the name:\t")
    if contact in contacts:
        contacts.pop(contact)
        print("Contact for", contact, "was deleted")
    else:
        print(contact + " doesn't exist\n")

def field(contacts):
    field = input("Please enter the field you want to view:\t").lower()
    print()
    contact=''
    for contact in contacts:
        
        if field == "address":
            key = "address"
            if key in contacts[contact]:
                address = contacts[contact][key]
                print(address, end='\n')
        
        elif field == "state":
            key = "state"
            if key in contacts[contact]:
                state = contacts[contact][key]
                print(state, end='\n')
        
        elif field == "company":
            key = "company"
            if key in contacts[contact]:
                company = contacts[contact][key]
                print(company, end='\n')
        
        elif field == "mobile":
            key = "mobile"
            if key in contacts[contact]:
                mobile = contacts[contact][key]
                print(mobile, end='\n')
                
        elif field == "landline":
            key = "landline"
            if key in contacts[contact]:
                landline = contacts[contact][key]
                print(landline, end='\n')

def main():
    contacts = {
        "Joel":                                                                                                    
            {"address": "1500 Anystreet, San Francisco, 94110",
             "company": "A startup",
             "mobile": "555-555-1111"},
        "Anne":
            {"address": "1000 Somestreet, Fresno, CA 93704",
             "state": "California",
             "landline": "125-555-2222",
             "company": "Some Great Company"},
        "Sally":
            {"state": "Illinois",
             "landline":"217-555-1222",
             "company": "Illinois Technologies",
             "mobile": "217-333-2353"},
        "Ben":
            {"address": "1400 Another Street, Fresno CA 93704",
             "state": "California",
             "mobile": "125-555-4444"}}

    print("Welcome to contacts manager program")
    print('''COMMAND MENU
              list - Display all contacts
              view - View a contact
              add - Add a contact
              del - Delete a contact
              field - View field for all contacts
              exit - Exit program\n''')
    
    command = input("Please enter the command:\t").lower()
    
    while (command != 'exit'):
        if command == "list":
            listContact(contacts)

        elif command == "view":
            viewContact(contacts)
            #viewContact(getContact(contacts))

        elif command == "add":
            addContact(contacts)

        elif command == "del":
            delContact(contacts)

        elif command == "field":
            field(contacts)
            
        else:
            command = input("Invalid Command!. Please enter the command:\t").lower()
       
    if command == "exit":
        print("Good bye")

if (__name__ == "__main__"):
    main()
