import tkinter as tk
from tkinter import ttk
from tkinter import *
from tkinter import messagebox
from speedingfine import SpeedingFineCalculator

#Class for the Presentation layer
class SpeedingFineFrame(ttk.Frame):
    def __init__(self, parent):

        ttk.Frame.__init__(self, parent,padding="10 10 10 10")
        # Business tier class
        #creating an object of SpeedingFineCalculator
        self.speedingFineCalculator = SpeedingFineCalculator()

        #Save the parent so we can call destroy on the parent window
        self.parent = parent
        
        # Define variables for text entry fields
        self.speedLimit= tk.DoubleVar()
        self.clockedSpeed= tk.DoubleVar()
        self.speedingFine= tk.DoubleVar()
       
        self.initComponents()
        
    def initComponents(self):
        self.pack()
        self.initMainFrame()
        self.initButtonsFrame()
        
 
    def initMainFrame(self):
        #Add implementation:
        #New Frame object and use grid method to add it to the parent frame (self).

        finesFrame = SpeedingFineFrame(self.parent)
        fineFrame.grid(row=0)

        '''Create and place the 6 labels and 3 text entry boxes'''
        ttk.Label(self.parent, text="Minimum Fine: $50").grid(column=0, row=0)      
        ttk.Label(self.parent,text="Penalty per MPH over limit: $5").grid(column=0, row=3)
        ttk.Label(self.parent,text="Penalty for 50 MPH over limit: $200").grid(column=0, row=5)

        ttk.Label(self.parent,text="Speed Limit: ").grid(column=0, row=12)
        ttk.Entry(self.parent, width=25, textvariable=self.speedLimit).grid(column=5, row=12, sticky=tk.E)
        
        ttk.Label(self.parent,text="Clock Speed: ").grid(column=0, row=15)
        ttk.Entry(self.parent, width=25, textvariable=self.clockedSpeed).grid(column=5, row=15, sticky=tk.E)
        
        ttk.Label(self.parent,text="Speeding fine: ").grid(column=0, row=18)
        ttk.Entry(self.parent, width=25, textvariable=self.speedingFine).grid(column=5, row=18, sticky=tk.E)
        
        #Connect the DoubleVar attributes declared in the constructor to the text entry boxes.
        self.parent.title(self.speedLimit.get())
        self.parent.title(self.clockedSpeed.get())
        self.parent.title(self.speedingFine.get())
              
    def initButtonsFrame(self):
        #Add implementation:
        #Creates a new Frame object and use grid method to add it to the parent frame (self).

        buttonsFrame = SpeedingFineFrame(self.parent)
        buttonsFrame.grid(row=22)
        
        #Create and place three buttons to this frame using grid method.
        ttk.Button(buttonsFrame,text="Calculate", command=self.calculate).grid(column=0, row=21, padx=5)
        ttk.Button(buttonsFrame,text="Clear", command=self.clear).grid(column=3, row=21)
        ttk.Button(buttonsFrame,text="Exit", command=self.parent.destroy).grid(column=5, row=21)
        
        #Add the corresponding event-handlers to the buttons.
        buttonsFrame.title(self.calculate.get())
        buttonsFrame.title(self.clear.get())
        buttonsFrame.title(self.parent.destroy.get())

        
    def calculateFine(self):
        #Add implementation
        # Event-handler for the ‘Calculate’ button.
        buttonsFrame.self.calculate.get()=calculateSpeedingFine(self.clockedSpeed)


        # Read the values of Entry boxes corresponding to the speeding limit and clocked speed.
        self.parent.title(self.speedLimit.get())
        self.parent.title(self.clockedSpeed.get())
        
        # call the calculateSpeedingFine method on self.speedingFineCalculator object to calculate the fine,
        
        
        # populate the text entry box with the fine. 
        self.speedLimit.set(self.speedLimit.get())
        self.clockedSpeed.set(self.clockedSpeed.get())
        self.speedingFine.set(self.calculate.get())

    def clear(self):
        # Add implementation: clear all the text entry boxes
        self.speedLimit.set(" ")
        self.clockedSpeed.set(" ")
        self.speedingFine.set(" ")
    
    def exit(self):
        self.parent.destroy()

def main():
    root = tk.Tk()
    root.title("Speeding Fine Calculator of Funnyville")
    root.geometry("600x400")
    
    SpeedingFineFrame(root)
    root.mainloop()

if __name__ == "__main__":
    main()
