##########################################################################
##           Project: Ticket class                                      ##
##           Cintia Celi                                                ##
##                                                                      ##
##########################################################################

## Ticket class that produces the actual ticket number
class Ticket:
    __lastSerialNumber = 11111  #private static variable initialized
    
    def __init__(self):
        Ticket.__lastSerialNumber += 1 #incrementing static variable bf assigning it to the attribute of Ticket
##        Ticket.__lastSerialNumber = __lastSerialNumber + 1
##        self.__lastSerialNumber = __lastSerialNumber + 1
        self.__serialNumber = Ticket.__lastSerialNumber # Assign private static variable

    ## Read only property to provide unique ticket numbers    
    @property 
    def serialNumber(self):
        return self.__serialNumber

    ## Read only property      
    @property 
    def price(self):
        return 0

    def __str__(self):
        return "Ticket Number: " + str(self.__serialNumber)

## WalkupTicket class / a subclass of the Ticket class
class WalkupTicket(Ticket):
    def __init__(self):
        Ticket.__init__(self)
        self.__price = 60

    ## Read only property that returns the price
    @property 
    def price(self):
        return self.__price
        
    def __str__(self):
        return "Walkup Ticket Number: " + str(super().__str__()) + "Price: $" + str(self.__price)


class AdvanceTicket(Ticket):
    def __init__(self, daysInAdvance):
        if daysInAdvance < 1:
            raise ValueError("days in advance should be greater or equal to 1")
      #  else:
            #Ticket.__init__(self)
        super().init__()
        self.daysInAdvance = daysInAdvance

        ## Read only property that returns the price specific to AdvanceTickets
    @property
    def price(self):
        #if self.daysInAdvance > 1 and daysInAdvance < 9:
##            self.__price = 50
##            return self.__price
##        elif daysInAdvance >= 10:
##            self.__price = 40
##            return self.__price

        if (self.daysInAdvance > 10):
            return 40
        else:
            return 50
        
    def __str__(self):
        return "Advance Ticket Number: " + str(super().__str__()) + "Price: $" + str(self.__price)
    

class StudentAdvanceTicket(AdvanceTicket):
    def __init__(self, daysInAdvance, studentID):
        AdvanceTicket.__init__(self, daysInAdvance)
        self.studentID = studentID

    ## Read only property that returns the price specific to StudentAdvancetickets
    @property
    def price(self):
           
        if daysInAdvance > 1 and daysInAdvance < 9:
            return ((super().price)/2)
        elif daysInAdvance >= 10:
            return ((super().price)/2)  

    def __str__(self):
        return "Student Advance Ticket Number: " + str(super().__str__()) + "Price: " + str(self.__price) + "Student ID: " + str(self.__studentID)

def main():
    walkups = WalkupTicket()

    adv1 = AdvanceTicket(1)
    adv2 = AdvanceTicket(20)

    s1 = StudentAdvanceTicket(8, 95001)
    s2 = StudentAdvanceTicket(12, 95022)

    print(walkups)
    print(adv1)
    print(adv2)
    print(s1)
    print(s2)
    
if __name__ == "__main__":
    main()
