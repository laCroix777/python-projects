# Class to represent a RadioBox which plays a radio station at a given
# frequency at a given volume.

class RadioBox:
    # 3 public static attributes
    volumeChange = 5 #represents the increase or decrease in volume.
    volumeMin = 0  #Minimum level of volume the radiobox can play at
    volumeMax = 20 #Maximum level of volume the radiobox can play at

    def __init__(self):
    # Add three private attributes
    #initializes the attributes of the radiobox
        self.__volume = 10      #Volume the radiobox is set at.
        self.__freq = 106.1     #The frequency the radiobox is set at.
        self.__isPlaying = False  #Attribute that indicates whether the box is playing  
    
#Add two public read-only properties called:
#volume, isPlaying  corresponding to the three private attributes.

    @property
    def volume(self):
        return self.__volume

    @property
    def isPlaying(self):
        return self.__isPlaying

## Public read-write property called frequency.
    
    @property
    def frequency(self):
        return self.__freq

    @frequency.setter
    def frequency(self, newFreq):
        if newFreq <= 0:
            raise ValueError("New frequency should be a positive number")
        else:
            self.__freq = newFreq
            print("New frequency has been set")
    
    def increaseVolume(self):
        if self.__volume >= volumeMax:
            raise ValueError("Max volume reached")
        else:
            self.__volume = (self.__volume + volumeChange)
            print("new volume is higher")

    def decreaseVolume(self):
        if (self.__volume <= 0):
            raise ValueError("Min volume is reached")
        else:
            self.__volume = (self.__volume - volumeChange)
            print("new volume is lower")

    def start(self):
        if self.__isPlaying == True:
            print("Already playing at ….", float(self.__freq))
        else:
            print("Started playing …." + float(self.__freq)

##    def stop(self):
##        if self.__isPlaying == False:
##            print("Already stopped")
##        elif self.__isPlaying == True:
##            self.__isPlaying = False
##            print("“Stopped playing”") 

def main():
    sep = '-'*40
    print(sep)

    rb = RadioBox()
    print("Created radio box with", rb.frequency, "frequency and at ", rb.volume, "volume.")
    print("At your service .............")

    cmd = input("'s' to start,\n'p' to stop,\n'i' to up the volume,\n'd' to lower volume.\n't' to tune\n'q' to exit\nyour input:  ").lower()
    while (cmd != 'q'):
        try: 
            if (cmd == 's'):
                rb.start()
            elif (cmd == 'p'):
                rb.stop()
            elif (cmd == 't'):
                newfreq = float(input("Enter the station frequency: "))
                rb.frequency = newfreq
            elif (cmd == 'i'):
                rb.increaseVolume()
            elif (cmd == 'd'):
                rb.decreaseVolume()
            else:
                print("Invalid command")
        except Exception as e:
            print(sep)
            print("*******Something went wrong: ",e, "*********")
        print(sep)
        print("Radiobox dashboard: Box running: ", 'YES' if rb.isPlaying else 'NO', "\t Freq:", rb.frequency, "\t Vol:", rb.volume)
        print(sep)
        cmd = input("'s' to start,\n'p' to stop,\n'i' to up the volume,\n'd' to lower volume.\n't' to tune\n'q' to exit\nyour input:  ").lower()
    print("Goodbye!")
    
if __name__ == "__main__":
    main()
